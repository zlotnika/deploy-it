module.exports = {
  "kind": "Service",
  "apiVersion": "v1",
  "metadata": {
    labels: {
      app: process.env.CI_ENVIRONMENT_SLUG,
      "ci-environment-slug": process.env.CI_ENVIRONMENT_SLUG,
    },
    "name": process.env.CI_ENVIRONMENT_SLUG,
  },
  "spec": {
    "ports": [
      {
        "name": "app",
        "port": 8080,
        "targetPort": 8080
      }
    ],
    "selector": {
      "app": process.env.CI_ENVIRONMENT_SLUG,
    }
  }
}
