const E = process.env;
const JIRA_VERSION_LINK_TEMPLATE = process.env.JIRA_VERSION_LINK_TEMPLATE || `https://breadwallet.atlassian.net/issues/?jql=fixVersion%20%3D%20__VERSION__`;

const m = E.VERSION.match(/^(\d+)(\.(\d+))?(\.(\d+))?/);
const jiraVersion = m && m[0];

hash = {
  text: `Deployed ${E.VERSION} of ${E.CI_PROJECT_NAME} to production.  Rollout typically takes a few minutes to complete.`,
  attachments: [{
    fallback: `Unable to link to things...`,
    actions: []
  }]
};

if(jiraVersion) {
  const VERSION=encodeURIComponent(jiraVersion);
  
  hash.attachments[0].actions.push({
    type: 'button',
    text: `${jiraVersion} in JIRA`,
    url: JIRA_VERSION_LINK_TEMPLATE.replace(/__VERSION__/g,VERSION),
  });
}

if(E.CI_ENVIRONMENT_URL) {
  hash.attachments[0].actions.push({
    type: 'button',
    text: 'Open in Browser',
    url: E.CI_ENVIRONMENT_URL
  });
}

module.exports = hash;
