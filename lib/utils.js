function parseQuery(query="") {
  const parts = query.split('&');
  const r = {};
  parts.forEach(p => {
    const [m,k,v] = p.match(/^(.*?)=(.*)$/) || [];
    if(k) r[decodeURIComponent(k)] = decodeURIComponent(v);
  });
  return r;
}

function objectToQuery(object={}) {
  const parts = [];
  for(var k in object) {
    let v = object[v];
    parts.push(`${encodeURIComponent(k)}=${encodeURIComponent(v)}`);
  }
  return parts.join('&');
}
